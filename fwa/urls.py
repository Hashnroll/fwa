from django.conf.urls import url
from django.urls import path
from fwa.apps.welcome.views import index

urlpatterns = [
    url('^$', index)
]
